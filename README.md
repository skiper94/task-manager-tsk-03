# TASK MANAGER

Console application for task list.

# DELEVELOPER INFO

NAME: Andrey Polyakov
E-MAIL: apolyakov@tsconsulting.com

# SOFTWARE

* JDK 15.0.1
* Windows 10 x64

# HARDWARE

* RAM 8Gb
* CPU i5
* HDD 500Gb

# RUN PROCESS

java -jar ./task-manager.jar

# SCREENSHOTS

https://yadi.sk/d/LxfcL9o3Ds5wGg?w=1
